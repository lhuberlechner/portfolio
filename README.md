# Portfolio

Hi, I'm Laura Huber-Lechner. 
This is my portfolio for software projects, developed during my training for Application-Development at CODERS.BAY Vienna. Currently I'm in Semester 3 of 4.

## RPG-Game with Java

This was the main project, and my personal favorite project so far, in Semester 2 / Java Advanced. With focus on algorithms and OOP, we implemented an CPU opponent for a Role Play / Strategy Game with medieval flair. There's a nifty damage triangle and a little bit of lore. 

You should check it out!

[Java Game](https://gitlab.com/LiVora/java-game-project)


## Frontend for Management Platform

The first Full Stack project in Semester 1 / Java Base is a management platform for CODERS.BAY Vienna to manage their students, trainers and timetables. Frontend developed with HTML, CSS, vue.js and the Backend is based on Java and Spring Boot, including a design prototype. 

On this project I worked primarily on the Frontend.

[Frontend Coders.Backend](https://gitlab.com/lhuberlechner/fullstackprojekt_lhl-dd )



## Other projects

### Webshop with PHP/Laravel

// current project for the final apprenticeship examination...


### Kotlin Movie Recommendation Platform 

The movie recommendation platform was the main focus for Semester 3 / Kotlin Advanced. Goal for this project was to work with Kotlin frameworks (Exposed, Dataframe, Ktor), software / database architecture and a webbased Dataset.

// Work in progress!


### Jetpack Compose Mobile App

During Semester 2 / Mobile Applications I developed an App for organizing and maintaining orchids. It features a list with all orchids, a Detailpage where you can see the plants details like its placement and watering cycle, also a calender with an overview of the watering days.

// Work in progress!

